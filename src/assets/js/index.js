'use strict';

const API_URL = 'https://rutas-turisticas.herokuapp.com';
const algorithmsData = {};

Chart.Legend.prototype.afterFit = function() {
    this.height = this.height + 10;
};

function getRandomColors(numberColors) {
    return new Array(numberColors)
        .fill(0)
        .map((_) => {
            const hex = Math.floor(Math.random() * 16777216).toString(16);
            const leftOffset = 6 - hex.length;
            return `#${new Array(leftOffset).fill('0').join('')}${hex}`;
        });
}

function buildChart(canvas, dataChart, chartOptions) {
    const ctx = canvas.getContext('2d');
    new Chart(ctx, {
        type: 'line',
        data: dataChart,
        options: chartOptions
    })
}


function fetchExecutionResults(algorithm) {
    return new Promise((resolve, reject) => {
        $.ajax({
            method: 'GET',
            url: `${API_URL}/algorithm-results/${algorithm}`,
            success: (response) => {
                resolve(response.data);
            },
            error: (err) => {
                console.log(`Error al obtener los resultados de ejecución de ${algorithm}`);
                console.error(err);
                reject(err);
            }
        })
    })
};

async function buildAlgorithmChart( { algorithm, measure, canvas } ) {
    if (!(algorithm in algorithmsData)) {
        algorithmsData[algorithm] = {
            resultsExecutions: await fetchExecutionResults(algorithm)
        }
    }

    const resultsExecutions = algorithmsData[algorithm].resultsExecutions;

    const iterations = Object.keys(resultsExecutions.reduce((iterations, resultExecution) => {
        if (!(resultExecution.testParams.iterations in iterations)) {
            iterations[resultExecution.testParams.iterations] = true;
        }
        return iterations;
    }, {})).map(v => Number(v));

    
    const testParamsIndice = {};
    const data = [];

    resultsExecutions.forEach((resultExecution) => {
        const testParams = { ...resultExecution.testParams }
        delete testParams.iterations;
        const testParamsStringified = JSON.stringify(testParams);
        if ( !(testParamsStringified in testParamsIndice) ) {
            testParamsIndice[testParamsStringified] = data.length;
            data.push({
                label: testParamsStringified,
                resultExecutions: [resultExecution] // mejorar fill iterations...
            });
        }
        else {
            const dataIndex = testParamsIndice[testParamsStringified];
            data[dataIndex].resultExecutions.push(resultExecution);
        }
    });

    if (!algorithmsData[algorithm].randomColors) {
        algorithmsData[algorithm].randomColors = getRandomColors(data.length);
    }
    const randomColors = algorithmsData[algorithm].randomColors;

    const dataChart = {
        labels: iterations,
        datasets: data.map((d, i) => {
            return {
                label: d.label.replace(/"/g, ''),
                fill: false,
                data: d.resultExecutions.map(resultExecution => resultExecution.averages[measure]),
                borderColor: randomColors[i],
                backgroundColor: randomColors[i]
            }
        })
    }
    buildChart(canvas, dataChart, {
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: measure == 'cost'? 'Coste' : 'Tiempo de ejecución (ms)'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Iteraciones'
                }
            }],
        }
    });
}

(async function() {
    if (document.body.classList.contains('dark-theme')) {
        Chart.defaults.global.defaultFontColor = 'white';
    }
    const resultsChartsElements = document.querySelectorAll('.algorithm-results-chart');

    for (let i = 0; i < resultsChartsElements.length; ++i) {
        const resultChartElement = resultsChartsElements[i];
        await buildAlgorithmChart({
            algorithm: resultChartElement.getAttribute('data-algorithm'),
            measure: resultChartElement.getAttribute('data-measure'),
            canvas: resultChartElement
        });
    }
})();

